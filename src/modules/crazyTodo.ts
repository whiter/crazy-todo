import { AnyAction } from "redux";
import produce from "immer";
import cuid from "cuid";
import { createSelector } from "reselect"
import { generateFakeTodos } from "../services/todoService";

export enum TodoItemType {
  Today = "Today",
  Then = "Then"
}

export type TodoItem = {
  id: string;
  content: string;
  due: TodoItemType;
};

export type AppState = {
  list: TodoItem[];
  newTodoItem: string;
  activeTab: TodoItemType;
};

const INPUT_CHANGED = "INPUT_CHANGED";
const INPUT_SUBMIT = "INPUT_SUBMIT";
const POSTPONE_ITEM = "POSTPONE_ITEM";
const SELECT_TAB = "SELECT_TAB";

export function inputSubmit() {
  return {
    type: INPUT_SUBMIT
  };
}

export function inputChange(payload: string) {
  return {
    type: INPUT_CHANGED,
    payload
  };
}

export function selectTab(payload: string) {
  return {
    type: SELECT_TAB,
    payload
  };
}

export function postponeItem(payload: string) {
  return {
    type: POSTPONE_ITEM,
    payload
  };
}

const initialState: AppState = {
  list: generateFakeTodos(1000),
  newTodoItem: "",
  activeTab: TodoItemType.Today
};

export default function reducer(state = initialState, action: AnyAction) {
  switch (action.type) {
    case INPUT_SUBMIT:
      return produce(state, draft => {
        draft.list = [
          ...state.list,
          { id: cuid(), content: state.newTodoItem, due: TodoItemType.Today }
        ];
        draft.newTodoItem = "";
      });

    case INPUT_CHANGED:
      return produce(state, draft => {
        draft.newTodoItem = action.payload;
      });

    case POSTPONE_ITEM:
      return produce(state, draft => {
        draft.list = state.list.map(item =>
          item.id !== action.payload
            ? item
            : { ...item, type: TodoItemType.Then }
        );
      });

    case SELECT_TAB:
      return produce(state, draft => {
        draft.activeTab = action.payload;
      });
    default:
      return state;
  }
}

const listSelector = (state: AppState) => state.list
export const todayCountSelector = createSelector(listSelector, list => list.filter(item => item.due === TodoItemType.Today).length)
export const thenCountSelector = createSelector(listSelector, list => list.filter(item => item.due === TodoItemType.Then).length)
