import cuid from "cuid";
import { TodoItemType } from "../modules/crazyTodo";

const modifiers = [
  "try to",
  "avoid",
  "skip",
  "pretend to",
  "help",
  "",
  "",
  "",
  "",
  ""
];

const verbs = [
  "learn",
  "clean",
  "buy",
  "pick",
  "do",
  "make",
  "fix",
  "exercise",
  "tweet",
  "promote",
  "code",
  "play",
  "find",
  "crash",
  "submit",
  "skip",
  "add",
  "forget",
  "avoid",
  "throw",
  "buy",
  "sell"
];

const nouns = [
  "Italian",
  "milk",
  "needle work",
  "chess",
  "Node.js",
  "fines",
  "books",
  "boots",
  "fishing rod",
  "distant relatives",
  "charges",
  "knife",
  "castle",
  "laptop",
  "principles",
  "adults",
  "bird"
];

function suffix(modifier: string) {
  return modifier === "avoid" || modifier === "skip" ? "ing" : "";
}

function addSuffix(verb: string, suffix: string) {
  if (!suffix) {
    return verb;
  }
  if (/e$/.test(verb)) {
    return verb.substr(0, verb.length - 1) + suffix;
  }
  return verb + suffix;
}

function pick(set: string[]) {
  return set[Math.floor(Math.random() * set.length)];
}

function generateTodo() {
  const modifier = pick(modifiers);
  const verb = pick(verbs);
  const ending = suffix(modifier);
  const noun = pick(nouns);

  return {
    content:
      modifier + (modifier ? " " : "") + addSuffix(verb, ending) + " " + noun,
    due: "Then",
    id: cuid()
  };
}

export function generateFakeTodos(n: number) {
  var items = new Array(n);
  var k;
  for (k = 0; k < n; k += 1) {
    items[k] = generateTodo();
  }
  return items;
}


