import React, { Component } from "react";
import { createStyles, Theme, WithStyles, withStyles } from "@material-ui/core";
import Header from "./components/Header";
import TodoInputContainer from "./containers/TodoInputContainer";
import TodosContainer from "./containers/TodosContainer";
import { AppState, thenCountSelector, todayCountSelector } from "./modules/crazyTodo";
import { connect } from "react-redux";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      width: "100%",
      height: "100vh",
      flexDirection: "column",
      justifyContent: "flex-start"
    },
    content: {
      maxWidth: 1024,
      width: 1024,
      marginLeft: "auto",
      marginRight: "auto",
      border: "1px solid gray",
      marginTop: theme.spacing.unit,
      marginBottom: theme.spacing.unit,
      height: "100%",
      display: "flex",
      flexDirection: "column",

    }
  });

type Props = WithStyles<typeof styles> & ReturnType<typeof mapStateToProps> ;

class App extends Component<Props> {
  render() {
    const { classes, notifications } = this.props;
    return (
      <div className={classes.root}>
        <Header title="🙃 Crazy todo list" notifications={notifications}/>
        <div className={classes.content}>
          <TodoInputContainer />
          <TodosContainer />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: AppState) => {
  const todayCount = todayCountSelector(state);
  const thenCount = thenCountSelector(state);

  return {
    notifications: [`Today: ${todayCount}`, `Then: ${thenCount}`]
  }
};

export default connect(mapStateToProps)(withStyles(styles)(App));
