import * as React from "react";
import {
  createStyles,
  WithStyles,
  Typography,
  Theme,
  withStyles,
  AppBar
} from "@material-ui/core";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    appBar: {
      padding: theme.spacing.unit * 2,
      display: "flex",
      justifyContent: "space-between",
      flexDirection: "row"
    },
    headerText: {
      color: theme.palette.background.paper,
      fontSize: 32,
    },
    notifications: {
      margin: 0,
      listStyle: "none",
      "& li": {
        display: "block",
        float: "left",
        paddingLeft: theme.spacing.unit * 4
      }
    }
  });

type OwnProps = {
  title: string;
  notifications: string[]
};

type Props = OwnProps & WithStyles<typeof styles>;

class Header extends React.Component<Props> {
  render() {
    const { classes, title } = this.props;
    return (
      <div className={classes.root}>
        <AppBar position="static" className={classes.appBar}>
          <Typography variant="h1" className={classes.headerText}>{title}</Typography>
          {this.renderNotifications()}
        </AppBar>
      </div>
    );
  }

  renderNotifications() {
    const { notifications, classes } = this.props;
    return (<ul className={classes.notifications}>
      {notifications.map((notification, i) => (<li key={`notification${i}`}>{notification}</li>))}
    </ul>)
  }
}

export default withStyles(styles)(Header);
