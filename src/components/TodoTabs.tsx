import * as React from "react";
import {
  createStyles,
  withStyles,
  WithStyles,
  Theme,
  Input,
  Button,
  Tabs,
  Tab
} from "@material-ui/core";
import cn from "classnames"
import { TodoItemType } from "../modules/crazyTodo";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      height: "100%",
      display: "flex",
      flexDirection: "column"
    },
    tabContent: {
      height: "100%",
      overflow: "auto"
    },
    hidden: {
      display: "none"
    }
  });

type OwnProps = {
  activeTab: TodoItemType;
  onChange: (value: TodoItemType) => void;
  todayTab: React.ReactNode;
  thenTab: React.ReactNode;
};

type Props = OwnProps & WithStyles<typeof styles>;

class TodoTabs extends React.Component<Props> {
  render() {
    const { classes, activeTab, onChange, todayTab, thenTab } = this.props;
    return (
      <div className={classes.root}>
        <Tabs value={activeTab} onChange={(_event: React.ChangeEvent<{}>, value: TodoItemType) => onChange(value)}>
          <Tab label="Today" value={TodoItemType.Today} />
          <Tab label="Then" value={TodoItemType.Then} />
        </Tabs>
        <div className={cn(classes.tabContent, { [classes.hidden]: activeTab === TodoItemType.Then })}>
          {todayTab}
        </div>
        <div className={cn(classes.tabContent, { [classes.hidden]: activeTab === TodoItemType.Today })}>
          {thenTab}
        </div>
      </div >
    );
  }
}

export default withStyles(styles)(TodoTabs);
