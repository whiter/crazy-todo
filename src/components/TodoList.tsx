import * as React from "react";
import {
  withStyles,
  WithStyles,
  Theme,
  createStyles
} from "@material-ui/core/styles";
import { TodoItem } from "../modules/crazyTodo";
import { ListItem, ListItemText, List } from "@material-ui/core";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      height: "100%",
      overflow: "auto"
    }
  });

type OwnProps = { list: TodoItem[] };
type Props = OwnProps & WithStyles<typeof styles>;

class TodoList extends React.Component<Props> {
  render() {
    const { classes, list } = this.props;
    return (
      <div className={classes.root}>
        <List>
          {list.map(item => (
            <ListItem key={item.id}>
              <ListItemText>{item.content}</ListItemText>
            </ListItem>
          ))}
        </List>
      </div>
    );
  }
}

export default withStyles(styles)(TodoList);
