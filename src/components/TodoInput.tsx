import * as React from "react";
import {
  createStyles,
  withStyles,
  WithStyles,
  Theme,
  Input,
  Button
} from "@material-ui/core";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      display: "flex",
      justifyContent: "space-between"
    },
    input: {
      flexGrow: 1
    }
  });

type OwnProps = {
  onChange: (value: string) => void;
  onSubmit: () => void;
  value: string;
};

type Props = OwnProps & WithStyles<typeof styles>;

class TodoInput extends React.Component<Props> {
  render() {
    const { classes, value, onSubmit} = this.props;
    return (
      <form onSubmit={this.handleFormSubmit}>
        <div className={classes.root}>
          <Input onChange={this.handleInputChange} value={value} className={classes.input} />
          <Button onClick={onSubmit} disabled={!value}>Add</Button>
        </div>
      </form>
    );
  }

  handleInputChange = (
    event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ) => {
    const { onChange } = this.props;
    onChange(event.target.value);
  };

  handleFormSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    const {onSubmit} = this.props
    event.preventDefault();
    onSubmit()
  }
}

export default withStyles(styles)(TodoInput);
