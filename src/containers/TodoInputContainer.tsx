import * as React from "react";
import { AppState, inputChange, inputSubmit } from "../modules/crazyTodo";
import { connect } from "react-redux";
import TodoInput from "../components/TodoInput";

type Props = ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps;

class TodoInputContainer extends React.Component<Props> {
  render() {
    const { value } = this.props;
    return <TodoInput value={value} onChange={this.handleChange} onSubmit={this.handleSubmit}/>;
  }

  handleChange = (value: string) => {
      this.props.inputChange(value)
  }

  handleSubmit = () => {
      this.props.inputSubmit()
  }
}

const mapStateToProps = (state: AppState) => ({
  value: state.newTodoItem
});

const mapDispatchToProps = {
    inputChange,
    inputSubmit
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoInputContainer);
