import * as React from "react";
import {
  AppState,
  inputChange,
  TodoItemType,
  selectTab
} from "../modules/crazyTodo";
import { connect } from "react-redux";
import TodoTabs from "../components/TodoTabs";
import TodoList from "../components/TodoList";

type Props = ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps;

class TodosContainer extends React.Component<Props> {
  render() {
    const { activeTab, thenTodos, todayTodos, selectTab } = this.props;
    return (
      <TodoTabs
        activeTab={activeTab}
        onChange={this.handleChange}
        todayTab={<TodoList list={todayTodos} />}
        thenTab={<TodoList list={thenTodos} />}
      />
    );
  }

  handleChange = (value: string) => {
    this.props.selectTab(value);
  };
}

const mapStateToProps = (state: AppState) => ({
  activeTab: state.activeTab,
  todayTodos: state.list.filter(todo => todo.due === TodoItemType.Today),
  thenTodos: state.list.filter(todo => todo.due === TodoItemType.Then)
});

const mapDispatchToProps = {
  selectTab
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TodosContainer);
